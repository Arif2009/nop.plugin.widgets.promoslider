﻿using Nop.Web.Framework.Mvc.Routes;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.PromoSlider.Infrastructure
{
    public class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get { return 1; }
        }

        public void RegisterRoutes(System.Web.Routing.RouteCollection routes)
        {
            ViewEngines.Engines.Insert(0, new PromoViewEngine());
        }
    }
}